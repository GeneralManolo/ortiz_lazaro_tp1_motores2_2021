﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Fuel : MonoBehaviour

    //Components needed to affect the Fuel Bar
{
    public static float fuelLeft = 15f;
    public Transform ui_fuel;
    public Text timerText;

    //I define how many fuel i have and where is the fuel bar
    void Start()
    {
        ui_fuel = GameObject.Find("Canvas/Fuel/Fuelbar").transform;
        fuelLeft = 25f;
    }

    void Update()
    {

        //Fuel left will determine how many fuel is remaining
        fuelLeft -= Time.deltaTime; 

        ui_fuel.localScale = new Vector3(fuelLeft, 1, 1);

        timerText.text = "Fuel remaining: " + fuelLeft.ToString("0");


        //This tells the game to throw a Gameover if the fuel is 0
        if (fuelLeft <= 0) 
        {
            Controller_Hud.gameOver = true;
        }
    }
}
