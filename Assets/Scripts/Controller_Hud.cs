﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    //This are necessary to show the corresponding text in the HUD.
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    public Text highscoreText;
    private float distance = 0;

    //At the start of the game, this values are predefined and will go up as the game progresses
    void Start()
    {
        gameOver = false;
        distance = 0;
        distanceText.text = distance.ToString("0");
        gameOverText.gameObject.SetActive(false);
        highscoreText.gameObject.SetActive(false);
    }

    //This will ask if the player met any conditions to lose the game
    void Update()
    {
        //The player hit an enemy
        if (gameOver)
        {
            Time.timeScale = 0;
            gameOverText.gameObject.SetActive(true);
            highscoreText.gameObject.SetActive(true);
            gameOverText.text = "Game Over. Press R to try again \n Total Distance: " + distance.ToString("0");
            highscoreText.text = "High Score: " + Controller_Persistence.instance.data.record.ToString("0");

            //I need this to tell the game wich value i want to save. The high score in this case
            if (Controller_Persistence.instance.data.record < distance)
            {

                Controller_Persistence.instance.data.record = distance;
                Controller_Persistence.instance.SavePersisnteceData();

            }

            }

        //The player doesn't hit an enemy
        else
        {
            distance += Time.deltaTime;
            distanceText.text = distance.ToString("0");
        }
    }
}
