﻿using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    //This componets will help me define the enemy's collision and velocity
    public static float enemyVelocity;
    private Rigidbody rb;

    //This just makes the enemy have a collision, needed to kill the player
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    //The enemies that are not loaded will, obviously, not move
    void Update()
    {
        rb.AddForce(new Vector3(-enemyVelocity, 0, 0), ForceMode.Force);
        OutOfBounds();
        DestroyEnemy();    
    }

    //When an enemy is no longer present on the screen, it will dissapear.
    public void OutOfBounds()
    {
        if (this.transform.position.x <= -15)
        {
            Destroy(this.gameObject);
        }
    }

    //Eliminates all enemies when the gameover function becomes true
    public void DestroyEnemy()
    {
        if (Controller_Hud.gameOver)
        {
            Destroy(this.gameObject);
        }
    }
}
