﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class Controller_Persistence : MonoBehaviour
{
    //i'll need this to make the Persistence work. It will allow me to save the values i need
    public static Controller_Persistence instance;
    public PersistenceData data;
    string dataArchive = "save.dat";

    private void Awake()
    {

        //It will destroy the persistence manager to secure that only one persistence manager exist at a time
        if (instance == null)
        {
            DontDestroyOnLoad(this.gameObject);
            instance = this;
        }
        else if (instance != this)
            Destroy(this.gameObject);

        LoadPersistenceData();
    }

    //Allows me to save the data i want to persist manually, at the press of a bottom.
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.G)) SavePersisnteceData();
    }


    //This is just the function that saves the data.
    public void SavePersisnteceData()
    {
        string filePath = Application.persistentDataPath + "/" + dataArchive;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Save successful!");
    }

    //This will load the data i persist previously
    public void LoadPersistenceData()
    {
        string filePath = Application.persistentDataPath + "/" + dataArchive;
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);
            PersistenceData cargado = (PersistenceData)bf.Deserialize(file);
            data = cargado;
            file.Close();
            Debug.Log("Load succesful!");
        }
    }

}

[System.Serializable]

//This right here is the data i want to persist so much
public class PersistenceData
{

    public float record;

    public PersistenceData()
    {

        record = 0;

    }
}


