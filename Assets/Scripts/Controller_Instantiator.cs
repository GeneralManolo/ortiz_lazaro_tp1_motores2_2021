﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour

    //This are needed to make the enemy exist
{
    public List<GameObject> enemies;
    public GameObject instantiatePos;
    public float respawningTimer;
    private float time = 0;

    //Controls the enemy's initial speed
    void Start()
    {
        Controller_Enemy.enemyVelocity = 2;
    }

    //calls every thick the SpawnEnemies and ChangeVelocity function
    void Update()
    {
        SpawnEnemies();
        ChangeVelocity();
    }

    //Makes the enemy spawn faster as the game progresses
    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(1f, 15f, time / 45f);
    }

    //Defines the enemies spawn and the wait time
    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(2, 6);
        }
    }
}
