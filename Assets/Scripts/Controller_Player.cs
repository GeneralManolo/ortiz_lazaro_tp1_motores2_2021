﻿using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    //this components will be needed to define the Player's actions
    private Rigidbody rb;
    public float jumpForce = 5;
    private float initialSize;
    private int i = 0;
    private bool floored;
    private bool fly;
    public float jetForce;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
    }

    void Update()
    {
        GetInput();
    }

    //I call the principal movements functions, necessary for gameplay
    private void GetInput()
    {
        Jump();
        Duck();
        Jet();
    }

    //The jump function will ask if the player is on the ground. If yes, it will jump
    private void Jump()
    {
        if (floored)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    //The duck function allows the player to shirnk
    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    //Allows the player to activate the Jetpack mechanic.
    private void Jet()
    {

        //I need to ask if the player is on the ground to activate the Jetpack.
        bool jet = Input.GetKey(KeyCode.W);

            if (!floored) 
        {
            fly = true;
        }
        if (floored)
        {
            fly = false;        
        }

        if (fly && jet)
        {
            rb.AddForce(Vector3.up * jetForce * Time.fixedDeltaTime, ForceMode.Acceleration);
            floored = false;
        }
    }

    //functions that requires the player to collide with things
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            //Destroys the player and the enemy when the two collides
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            //This allows me to check that the player is still on the ground. Very useful
            floored = true;
        }

        if (collision.gameObject.CompareTag("Fuel")) 
        {
            //This makes the Fuel power up work properly
            Controller_Fuel.fuelLeft += 15f;
            Destroy(collision.gameObject);
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }
}
