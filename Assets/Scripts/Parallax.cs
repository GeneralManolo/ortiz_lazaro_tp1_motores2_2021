﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    //i iniciate what i need to create the parallax effect
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;

    //I define the parallax starting position and leght
    void Start()
    {
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    //makes the parallax possible
    void Update()
    {
        transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z);
        if (transform.localPosition.x < -20)
        {
            transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z);
        }

        //Allows me to stop the parallax effect when the gameover function becomes true
        if(Controller_Hud.gameOver)
        {
            parallaxEffect = 0;
        }
    }
}
